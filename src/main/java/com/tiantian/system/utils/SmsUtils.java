package com.tiantian.system.utils;

import com.alibaba.fastjson.JSON;
import com.tiantian.system.proxy_client.NotifierIface;
import com.tiantian.system.thrift.notifier.NoticeType;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class SmsUtils {

    public static void sendMessage(String mobile, String msg, SmsType type) {
        msg ="【波克赢家】" + msg;
        Map<String ,String> map = new HashMap<>();
        map.put("mobile", mobile);
        map.put("msgContent", msg);
        map.put("corpMsgId", "");
        map.put("ext", "");
        map.put("type", type.value());
        map.put("shortCode", "");
        Map<String, String> messageMap = generate("single", JSON.toJSONString(map));
        try {
            // 分发到game的队列中
            NotifierIface.instance().iface().notify(NoticeType.SMS.getValue(), "GmbackService", messageMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> generate(String type, String data) {
        Map<String, String> map = new HashMap<>();
        map.put("type", type);
        map.put("data", data);
        return map;
    }
}
