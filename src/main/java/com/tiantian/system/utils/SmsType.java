package com.tiantian.system.utils;

/**
 *
 */
public enum SmsType {
    REGISTER("register_code"), // 注册验证码
    INVITE_FRIEND("invite_friend"), //邀请好友下载游戏
    JD_ECARD("jd_ecard_notice"); // 京东卡兑换

    private String value;
    SmsType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

}
