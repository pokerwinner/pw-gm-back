package com.tiantian.service.proxy_client;

import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.service.settings.RoomConfig;
import com.tiantian.service.thrift.room.RoomService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class RoomIface extends IFace<RoomService.Iface>{
    private static class RoomIfaceHolder {
        private final static RoomIface instance = new RoomIface();
    }

    private RoomIface() {
    }

    public static RoomIface instance() {
        return RoomIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new RoomService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(RoomConfig.getInstance().getHost(),
                RoomConfig.getInstance().getPort());
    }

    @Override
    protected Class<RoomService.Iface> faceClass() {
        return RoomService.Iface.class;
    }
}
