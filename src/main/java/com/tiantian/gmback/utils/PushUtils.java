package com.tiantian.gmback.utils;

import cn.jpush.api.JPushClient;
import cn.jpush.api.common.ClientConfig;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.PushPayload;

/**
 *
 */
public class PushUtils {
    private static final String MASTER_SECRET = "2cbb460f293fda5c7491cc51";
    private static final String APP_KEY = "5b0a44aa0f0cc9a8c18f0525";
    public static void sendToAll(String message) {
        ClientConfig config = ClientConfig.getInstance();
        config.setMaxRetryTimes(5);
        config.setConnectionTimeout(30 * 1000);	// 10 seconds
        config.setSSLVersion("TLSv1.1");		// JPush server supports SSLv3, TLSv1, TLSv1.1, TLSv1.2
        config.setApnsProduction(true); 	// development env
        JPushClient jPushClient = new JPushClient(MASTER_SECRET, APP_KEY, null, config);
        PushPayload payload = buildPushObjectToAll(message);

        try {
            PushResult result = jPushClient.sendPush(payload);
            System.out.println("Got result - " + result);

        } catch (APIConnectionException e) {
            // Connection error, should retry later
            System.out.println("Connection error, should retry later");

        } catch (APIRequestException e) {
            // Should review the error, and fix the request
            System.out.println("Should review the error, and fix the request");
            System.out.println("HTTP Status: " + e.getStatus());
            System.out.println("Error Code: " + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMessage());
        }
    }
    public static PushPayload buildPushObjectToAll(String message) {
        return PushPayload.alertAll(message);
    }
}
