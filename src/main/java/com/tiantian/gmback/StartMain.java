package com.tiantian.gmback;

import com.tiantian.gmback.application.ControllerApplication;
import ro.pippo.core.Pippo;

/**
 *
 */
public class StartMain {
    public static void main(String[] args) {
        Pippo pippo = new Pippo(new ControllerApplication());
        pippo.start();
    }
}
