package com.tiantian.gmback.service;

import com.tiantian.gmback.db.Page;
import com.tiantian.gmback.entity.Notice;

/**
 *
 */
public interface NoticeService {
    boolean addNotice(Notice notice);

    boolean updateNotice(Notice notice);

    boolean deleteNotice(String id);

    Page<Notice> getPage(Page<Notice> page, Notice searchNotice);

    boolean loadNoticesToCache();
}
