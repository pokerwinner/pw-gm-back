package com.tiantian.gmback.service;

import com.tiantian.gmback.controller.ResultStatus;
import com.tiantian.gmback.db.Page;
import com.tiantian.gmback.entity.order.Order;

/**
 *
 */
public interface OrderService {
   Page<Order> orderPage(Page<Order> page, Order search);
   ResultStatus sendJdEcard(String orderId, String ecardCode);
}
