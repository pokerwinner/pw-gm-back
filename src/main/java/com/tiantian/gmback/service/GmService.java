package com.tiantian.gmback.service;

/**
 *
 */
public interface GmService {

    void setAppStatus(String appName, long beginDate, long hours);

    void delAppStatus(String appName);
}
