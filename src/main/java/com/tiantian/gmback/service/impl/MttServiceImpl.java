package com.tiantian.gmback.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.tiantian.gmback.db.JdbcTemplate;
import com.tiantian.gmback.entity.mtt.*;
import com.tiantian.gmback.service.MttService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.util.Assert;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.*;

/**
 *
 */
public class MttServiceImpl implements MttService {
    @Inject
    @Named("mongoCore")
    MongoTemplate mongoTemplate;
    @Inject
    private JdbcTemplate jdbcTemplate;

    private Map<String, List<MttRule> > mttRulesMap;

    public MttServiceImpl() {
        init();
    }
    private void init() {
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream("config/mtt_rules.json");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        List<RuleInfo> ruleInfos = JSON.parseArray(configString, RuleInfo.class);
        mttRulesMap = new HashMap<>();
        if (ruleInfos != null && ruleInfos.size() > 0) {
            for(RuleInfo ruleInfo : ruleInfos) {
                mttRulesMap.put(ruleInfo.getRuleId(), ruleInfo.getRules());
            }
        }
    }



    @Override
    public MttGame saveMttGame(String gameName, long taxFee, long poolFee, int tableUsers, int minUsers, int maxUsers,
                               long startMills, int perRestMins, int restMins, int startBuyIn, int buyInCnt,
                               String rebuyDesc, int canRebuyBlindLvl, int signDelayMins, String rewardMark,
                               String type, String ruleId, long[] virtualNums, String[] physicalIds) {
        MttGame mttGame = new MttGame();
        mttGame.setGameName(gameName);
        mttGame.setTaxFee(taxFee);
        mttGame.setPoolFee(poolFee);
        mttGame.setTableUsers(tableUsers);
        mttGame.setMinUsers(minUsers);
        mttGame.setMaxUsers(maxUsers);
        mttGame.setStartMills(startMills);
        mttGame.setPerRestMins(perRestMins);
        mttGame.setRestMins(restMins);
        mttGame.setStartBuyIn(startBuyIn);
        mttGame.setRebuyDesc(rebuyDesc);
        mttGame.setCanRebuyBlindLvl(canRebuyBlindLvl);
        mttGame.setSignDelayMins(signDelayMins);
        mttGame.setRewardMark(rewardMark);
        mttGame.setType(type);
        mttGame.setAvailable(0);
        mttGame.setCurretBlindLvl(-1);
        mttGame.setStatus(0);
        mttGame.setBuyInCnt(buyInCnt);
        mttGame.setCreateDate(System.currentTimeMillis());
        List<MttReward> mttRewardList = new ArrayList<>();
        if (physicalIds != null) {
            int a = 0;
            for (String physicalId : physicalIds) {
                 a++;
                 MttReward mttReward = new MttReward();
                 Reward reward = getReward(physicalId);
                 if (reward == null) {
                     continue;
                 }
                 mttReward.setRanking(a);
                 mttReward.setPhysicalId(physicalId);
                 mttReward.setPhysicalName(reward.getAwardName());
                 mttRewardList.add(mttReward);
            }
        }
        if (virtualNums != null) {
            int i = 0;
            for (long virtualNum : virtualNums) {
                 MttReward mttReward = mttRewardList.get(i);
                 if(mttReward == null) {
                    mttReward = new MttReward();
                    mttReward.setRanking(i + 1);
                    mttRewardList.add(mttReward);
                 }
                 mttReward.setVirtualType(1);
                 mttReward.setVirtualNums(virtualNum);
                 i++;
            }
        }
        mttGame.setRewards(mttRewardList);
        if (StringUtils.isBlank(ruleId)) {
            ruleId = "1";
        }
        List<MttRule> mttRules = mttRulesMap.get(ruleId);
        mttGame.setRules(mttRules);

        mttGame.setMttTableUsers(new ArrayList<>());
        mongoTemplate.save(mttGame);
        return mttGame;
    }


    public Reward addNewReward(String awardName) {
        Assert.notNull(awardName);
        String sql = "insert into mtt_award(award_id, award_name, status, create_time)" +
                     "values(?,?,?,?)";
        try {
            return jdbcTemplate.insertBean(sql, Reward.class, UUID.randomUUID().toString().replace("-", ""),
                    awardName, 1, System.currentTimeMillis());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Reward getReward(String awardId)   {
        try {
            return jdbcTemplate.queryBean("select * from mtt_award where award_id = ?", Reward.class, awardId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
