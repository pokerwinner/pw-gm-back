package com.tiantian.gmback.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.inject.Inject;
import com.tiantian.gmback.db.IDUtils;
import com.tiantian.gmback.db.JdbcTemplate;
import com.tiantian.gmback.db.Page;
import com.tiantian.gmback.db.tables.gm.NoticesTable;
import com.tiantian.gmback.entity.Notice;
import com.tiantian.gmback.service.NoticeService;
import org.springframework.data.redis.core.StringRedisTemplate;
import java.sql.SQLException;
import java.util.List;

/**
 *
 */
public class NoticeServiceImpl implements NoticeService {
    @Inject
    private JdbcTemplate jdbcTemplate;
    @Inject
    private StringRedisTemplate stringRedisTemplate;

    private static final String NOTICE_KEY = "notice_key:";


    public boolean addNotice(Notice notice) {
        notice.setId(IDUtils.UUID());
        String sql = String.format("insert into %s (%s, %s, %s, %s, %s, %s, %s, %s) values(?, ?, ?, ?, ?, ?, ?, ?)",
                NoticesTable.TABLE_NOTICE, NoticesTable.COLUMN_ID, NoticesTable.COLUMN_TITLE, NoticesTable.COLUMN_CONTENT,
                NoticesTable.COLUMN_TAIL, NoticesTable.COLUMN_BEGIN_DATE, NoticesTable.COLUMN_END_DATE, NoticesTable.COLUMN_AVAILABLE,
                NoticesTable.COLUMN_SHOW_ORDER);
        try {
            Notice $notice = jdbcTemplate.insertBean(sql, Notice.class, notice.getId(), notice.getTitle(), notice.getContent(),
                    notice.getTail(), notice.getBeginDate(), notice.getEndDate(), notice.isAvailable(), notice.getShowOrder());
            return $notice != null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateNotice(Notice notice) {
        String sql = "update %s set %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? where %s = ?";
        try {
            int ret = jdbcTemplate.update(sql, NoticesTable.TABLE_NOTICE, NoticesTable.COLUMN_TITLE, NoticesTable.COLUMN_CONTENT,
                    NoticesTable.COLUMN_TAIL, NoticesTable.COLUMN_BEGIN_DATE, NoticesTable.COLUMN_END_DATE,
                    NoticesTable.COLUMN_AVAILABLE, NoticesTable.COLUMN_SHOW_ORDER, NoticesTable.COLUMN_ID, notice.getTitle(),
                    notice.getContent(), notice.getBeginDate(), notice.getEndDate(), notice.isAvailable(), notice.getShowOrder(),
                    notice.getId());
            return ret > 0;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteNotice(String id) {
        String sql = String.format("delete from %s where %s = ?", NoticesTable.TABLE_NOTICE, NoticesTable.COLUMN_ID);
        try {
            int ret = jdbcTemplate.update(sql, id);
            return ret > 0;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Page<Notice> getPage(Page<Notice> page, Notice searchNotice) {
        String sql = String.format("select * from %s", NoticesTable.TABLE_NOTICE);
        try {
            return jdbcTemplate.getPage(sql, page, Notice.class);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return page;
    }

    public boolean loadNoticesToCache() {
        List<Notice> noticeList = getAllAvailablePages();
        stringRedisTemplate.opsForValue().set(NOTICE_KEY, JSON.toJSONString(noticeList));
        return true;
    }

    private List<Notice> getAllAvailablePages()
    {
        String sql = String.format("select * from %s where %s = ? and %s > %d order by %s", NoticesTable.TABLE_NOTICE,
                NoticesTable.COLUMN_AVAILABLE, NoticesTable.COLUMN_END_DATE, System.currentTimeMillis(), NoticesTable.COLUMN_SHOW_ORDER);
        try {
            return jdbcTemplate.queryBeanList(sql, Notice.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
