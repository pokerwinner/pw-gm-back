package com.tiantian.gmback.service.impl;
import com.alibaba.fastjson.JSON;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.tiantian.gmback.db.JdbcTemplate;
import com.tiantian.gmback.db.Page;
import com.tiantian.gmback.entity.Group;
import com.tiantian.gmback.entity.Room;
import com.tiantian.gmback.entity.UserShellLogs;
import com.tiantian.gmback.service.IRoomService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.sql.SQLException;
import java.util.List;

/**
 *
 */
public class RoomService implements IRoomService {
    @Inject
    JdbcTemplate jdbcTemplate;

    @Inject
    @Named("mongoCore")
    MongoTemplate mongoTemplate;

    @Inject
    @Named("mongoLogs")
    MongoTemplate logMongoTemplate;

    @Inject
    StringRedisTemplate strRedisTemplate;

    @Override
    public Room getRoom(String id) {
        try {
            System.out.println(JSON.toJSONString(jdbcTemplate.queryBean("select * from users", Room.class)));
            List<Room> $room = jdbcTemplate.queryBeanList("select * from rooms", Room.class);
            System.out.println(JSON.toJSONString($room));
            Page<Room> page = new Page<>();
            page.setPageSize(2);
            page = jdbcTemplate.getPage("select * from rooms", page, Room.class);
            System.out.println(JSON.toJSONString(page));
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        List<UserShellLogs> list = logMongoTemplate.find(new Query(), UserShellLogs.class);
        System.out.println(JSON.toJSONString(list));
        String result = strRedisTemplate.opsForValue().get("app_status:");
        System.out.println(result);

        Room room = new Room();
        room.setRoomId(id);
        room.setRoomName("room");
        return room;
    }
}
