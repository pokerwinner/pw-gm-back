package com.tiantian.gmback.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.tiantian.gmback.entity.MaintainInfo;
import com.tiantian.gmback.service.GmService;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import java.util.List;

/**
 *
 */
public class GmServiceImpl implements GmService {
    private static final String MAINTAIN_INFO_KEY = "maintain_info:";
    @Inject
    StringRedisTemplate strRedisTemplate;

    public void setAppStatus(String appName, long beginDate, long hours) {
        String jsonStr = strRedisTemplate.opsForValue().get(MAINTAIN_INFO_KEY);
        List<MaintainInfo> maintainInfoList = null;
        if (jsonStr == null) {
            MaintainInfo maintainInfo = new MaintainInfo();
            maintainInfo.setAppName(appName);
            maintainInfo.setBeginDate(beginDate);
            maintainInfo.setHours(hours);
            maintainInfoList = Lists.newArrayList(maintainInfo);
        }
        else {
            maintainInfoList = JSON.parseArray(jsonStr, MaintainInfo.class);
            boolean has = false;
            for (MaintainInfo maintainInfo : maintainInfoList) {
                 if (maintainInfo.getAppName().equalsIgnoreCase(appName)) {
                     maintainInfo.setBeginDate(beginDate);
                     maintainInfo.setHours(hours);
                     has = true;
                 }
            }
            if (!has) {
                MaintainInfo maintainInfo = new MaintainInfo();
                maintainInfo.setAppName(appName);
                maintainInfo.setBeginDate(beginDate);
                maintainInfo.setHours(hours);
                maintainInfoList.add(maintainInfo);
            }
        }
        strRedisTemplate.opsForValue().set(MAINTAIN_INFO_KEY, JSON.toJSONString(maintainInfoList));
    }

    public void delAppStatus(String appName) {
        String jsonStr = strRedisTemplate.opsForValue().get(MAINTAIN_INFO_KEY);
        if (jsonStr != null) {
            List<MaintainInfo> maintainInfoList = JSON.parseArray(jsonStr, MaintainInfo.class);
            if (maintainInfoList.size() > 0) {
                for (int i = 0; i < maintainInfoList.size(); i++) {
                    MaintainInfo maintainInfo = maintainInfoList.get(i);
                    if (maintainInfo.getAppName().equalsIgnoreCase(appName)) {
                        maintainInfoList.remove(i);
                        break;
                    }
                }
                strRedisTemplate.opsForValue().set(MAINTAIN_INFO_KEY, JSON.toJSONString(maintainInfoList));
            }
        }
    }
}
