package com.tiantian.gmback.service.impl;

import com.google.inject.Inject;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.gmback.controller.ResultStatus;
import com.tiantian.gmback.db.IDUtils;
import com.tiantian.gmback.db.JdbcTemplate;
import com.tiantian.gmback.db.Page;
import com.tiantian.gmback.db.tables.OrdersTable;
import com.tiantian.gmback.db.tables.logs.JdEcardLogsTable;
import com.tiantian.gmback.db.tables.order.OrderAddressTable;
import com.tiantian.gmback.entity.logs.JdEcardLogs;
import com.tiantian.gmback.entity.order.Order;
import com.tiantian.gmback.entity.order.OrderAddress;
import com.tiantian.gmback.service.OrderService;
import com.tiantian.system.utils.SmsType;
import com.tiantian.system.utils.SmsUtils;
import org.apache.thrift.TException;

import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 */
public class OrderServiceImpl implements OrderService {
    @Inject
    private JdbcTemplate jdbcTemplate;

    // 发送邮件和短消息消息线程
    private ExecutorService service = Executors.newFixedThreadPool(5);

    public Page<Order> orderPage(Page<Order> page, Order search) {
        String sql = String.format("select * from %s", OrdersTable.TABLE_ORDER);
        try {
            return jdbcTemplate.getPage(sql, page, Order.class);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return page;
    }

    // 话费发货
    public void sendHF(String orderId) {
       Order order = getOrderById(orderId);
       if (order == null) {
          // 订单信息不存在
       }
       if ("2".equalsIgnoreCase(order.getStatus())) {
            // 已经发货
       }
       updateOrderStatus(orderId, "2");

    }

    // 批量话费发货
    public void sendHFBatch(String[] orderNos) {

    }

    // 发货京东卡
    public ResultStatus sendJdEcard(String orderId, String ecardCode) {
        Order order = getOrderById(orderId);
        if (order == null) {
            // 订单信息不存在
            return new ResultStatus(100, "订单信息不存在");
        }
        if ("2".equalsIgnoreCase(order.getStatus())) {
            // 已经发货
            return new ResultStatus(101, "订单已经发货");
        }
        UserDetail userDetail = null;
        try {
            userDetail = AccountIface.instance().iface().findUserDetailByUserId(order.getUserId());
        } catch (TException e) {
            e.printStackTrace();
        }
        if (userDetail == null) {
            return new ResultStatus(102, "下单用户信息不存在");
        }
        String mobile = userDetail.getMobile();

        OrderAddress orderAddress = getOrderAddressById(order.getAddressId());
        if(orderAddress == null) {
           return new ResultStatus(103, "订单发货地址不存在");
        }
        String email = orderAddress.getEmail();
        updateOrderStatus(orderId, "2");
        // 生成记录
        JdEcardLogs jdEcardLogs = new JdEcardLogs();
        jdEcardLogs.setId(IDUtils.UUID());
        jdEcardLogs.setOrderId(orderId);
        jdEcardLogs.setUserId(order.getUserId());
        jdEcardLogs.setEcardCode(ecardCode); //TODO 加密
        jdEcardLogs.setSecretId("1"); //TODO 设置秘钥ID
        jdEcardLogs.setCreateDate(System.currentTimeMillis());
        saveEcardLog(jdEcardLogs);
        asyncSend(email, ecardCode, mobile);

        return new ResultStatus(1, "发货成功");
    }

    private Order getOrderById(String orderId) {
        String sql = String.format("select * from %s where %s = ?", OrdersTable.TABLE_ORDER,
                OrdersTable.COLUMN_ORDER_ID);
        try {
            return jdbcTemplate.queryBean(sql, Order.class, orderId);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean updateOrderStatus(String orderId, String status) {
        String sql = String.format("update %s set %s = ?, %s = ? where %s = ?", OrdersTable.TABLE_ORDER,
                OrdersTable.COLUMN_STATUS, OrdersTable.COLUMN_UPDATE_DATE, OrdersTable.COLUMN_ORDER_ID);
        try {
            return jdbcTemplate.update(sql, status, System.currentTimeMillis(), orderId) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean saveEcardLog(JdEcardLogs jdEcardLogs) {
        String sql = String.format("insert into %s(%s, %s, %s, %s, %s, %s) values(?, ?, ?, ?, ?, ?)",
             JdEcardLogsTable.TABLE_JD_ECARD_LOGS, JdEcardLogsTable.COLUMN_ID, JdEcardLogsTable.COLUMN_ORDER_ID,
             JdEcardLogsTable.COLUMN_USER_ID, JdEcardLogsTable.COLUMN_ECARD_CODE, JdEcardLogsTable.COLUMN_SECRET_ID,
             JdEcardLogsTable.COLUMN_CREATE_DATE);
        try {
            JdEcardLogs retJdEcardLogs = jdbcTemplate.insertBean(sql, JdEcardLogs.class, jdEcardLogs.getId(),
                    jdEcardLogs.getOrderId(), jdEcardLogs.getUserId(), jdEcardLogs.getEcardCode(), jdEcardLogs.getSecretId(),
                    jdEcardLogs.getCreateDate());
            return retJdEcardLogs != null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean sendEmail(String email, String ecardCode) {
        return false;
    }

    private boolean sendMessage(String mobile) {
        String message = "你兑换的京东e卡的使用码已经发送到你的邮箱,请注意查收";
        SmsUtils.sendMessage(mobile, message, SmsType.JD_ECARD);
        return true;
    }

    private OrderAddress getOrderAddressById(String addressId) {
        String sql = String.format("select * from %s where %s = ?",
                OrderAddressTable.TABLE_ORDER_ADDRESS, OrderAddressTable.COLUMN_ADDRESS_ID);
        try {
            return jdbcTemplate.queryBean(sql, OrderAddress.class, addressId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void asyncSend(final String email, final String ecardCode, final String mobile) {
        // 异步发送邮件和短消息
        service.execute(new Runnable() {
            @Override
            public void run() {
                //TODO 发送ecardCode到邮箱和手机短信通知
                sendEmail(email, ecardCode);
                sendMessage(mobile);
            }
        });
    }
}
