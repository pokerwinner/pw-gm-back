package com.tiantian.gmback.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.inject.Inject;
import com.tiantian.gmback.db.JdbcTemplate;
import com.tiantian.gmback.db.tables.gm.ChargeInfoTable;
import com.tiantian.gmback.entity.ChargeInfo;
import com.tiantian.gmback.service.ReChargeService;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.sql.SQLException;
import java.util.List;

/**
 *
 */
public class ReChargeServiceImpl implements ReChargeService {
    @Inject
    private JdbcTemplate jdbcTemplate;
    @Inject
    private StringRedisTemplate stringRedisTemplate;

    private static final String CHARGE_INFO_KEY = "charge_info_key";

    public boolean loadRechargeToCache() {
        String sql = String.format("select * from %s", ChargeInfoTable.TABLE_CHARGE_INFO);
        try {
            List<ChargeInfo> result = jdbcTemplate.queryBeanList(sql, ChargeInfo.class);
            if (result != null) {
                stringRedisTemplate.opsForValue().set(CHARGE_INFO_KEY, JSON.toJSONString(result));
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
