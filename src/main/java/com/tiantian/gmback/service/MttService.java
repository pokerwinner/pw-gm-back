package com.tiantian.gmback.service;

import com.tiantian.gmback.entity.mtt.MttGame;
import com.tiantian.gmback.entity.mtt.Reward;

/**
 *
 */
public interface MttService {
      MttGame saveMttGame(String gameName, long taxFee, long poolFee,
                          int tableUsers, int minUsers, int maxUsers,
                          long startMills, int perRestMins, int restMins,
                          int startBuyIn, int buyInCnt, String rebuyDesc,
                          int canRebuyBlindLvl, int signDelayMins,
                          String rewardMark, String type, String ruleId,
                          long[] virtualNums, String[] physicalIds);

      Reward addNewReward(String rewardName);
}
