package com.tiantian.gmback.service;

import com.tiantian.gmback.entity.Room;

/**
 *
 */
public interface IRoomService {
    Room getRoom(String id);
}
