package com.tiantian.gmback.mongo;

import com.google.inject.Inject;
import com.mongodb.Mongo;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.inject.Provider;


/**
 *
 */
public class LogMongoTemplateProvider implements Provider<MongoTemplate> {

    private MongoTemplate mongoTemplate;

    @Inject
    public LogMongoTemplateProvider(Mongo mongo) {
        mongoTemplate = new MongoTemplate(mongo, "pw_logs");
    }

    @Override
    public MongoTemplate get() {
        return mongoTemplate;
    }
}
