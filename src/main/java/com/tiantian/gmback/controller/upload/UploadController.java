package com.tiantian.gmback.controller.upload;

import com.tiantian.gmback.controller.ResultStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.pippo.controller.Controller;
import ro.pippo.core.FileItem;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 */
public class UploadController  extends Controller {
    private Logger LOG = LoggerFactory.getLogger(UploadController.class);
    private static final String FILE_PATH = "/home/winner/logs/";
//    private static final String FILE_PATH = "/Users/lichenghu/Documents/logs/";

    public void uploadLog() {
        FileItem file = getRouteContext().getRequest().getFile("file");
        try {
            String path = FILE_PATH + day();
            //  生成目录
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    LOG.error("create file dir fail");
                    getResponse().send("0");
                   // getResponse().json().send(new ResultStatus());
                    return;
                }
            }
            File uploadedFile = new File(path + "/" + System.currentTimeMillis() +"_" + file.getSubmittedFileName());
            try {
                if (!uploadedFile.exists()) {
                    boolean result = uploadedFile.createNewFile();
                    if (!result) {
                        LOG.error("create file dir fail");
                        getResponse().send("0");
                        return;
                    }
                }
                file.write(uploadedFile);
                getResponse().send("0");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {
            getResponse().send("0");
        }
    }

    private String day() {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        return sf.format(new Date());
    }
}
