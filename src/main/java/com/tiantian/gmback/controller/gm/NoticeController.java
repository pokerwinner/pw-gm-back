package com.tiantian.gmback.controller.gm;

import com.google.inject.Inject;
import com.tiantian.gmback.db.Page;
import com.tiantian.gmback.entity.Notice;
import com.tiantian.gmback.service.NoticeService;
import ro.pippo.controller.Controller;
import ro.pippo.controller.Form;
import ro.pippo.core.Param;

/**
 *
 */
public class NoticeController extends Controller {
    @Inject
    private NoticeService noticeService;

    public void addNotice(@Form Notice notice) {
        boolean ret = noticeService.addNotice(notice);
        getResponse().json().send(ret);
    }

    public void updateNotice(@Form Notice notice) {
        noticeService.updateNotice(notice);
        getResponse().json().send(notice);
    }

    public void loadAllNoticeToCache() {
        noticeService.loadNoticesToCache();
        getResponse().json().send(true);
    }

    public void deleteNotice(@Param("id") String id) {
        noticeService.deleteNotice(id);
        getResponse().json().send(true);
    }

    public void noticePage(@Form Page<Notice> noticePage, @Form Notice notice) {
       noticePage = noticeService.getPage(noticePage, notice);
       getResponse().json().send(noticePage);
    }
}
