package com.tiantian.gmback.controller.gm;

import com.tiantian.gm.proxy_client.GmIface;
import com.tiantian.gm.thrift.WinesResponse;
import org.apache.thrift.TException;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class WinesController extends Controller {

    // 添加物品
    public void addWine(@Param("code") String code,
                         @Param("name") String name,
                         @Param("img_url") String imgUrl,
                         @Param("desc") String desc,
                         @Param("cost_diamond") long costDiamond,
                         @Param("gift_money") long giftMoney,
                         @Param("avaliable") String avaliable) {
        WinesResponse response = null;
        try {
            response = GmIface.instance().iface().addWines(code, name, imgUrl, desc, costDiamond,
                    giftMoney, avaliable);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(response);
    }

    // 更新物品
    public void updateWine(@Param("code") String code,
                            @Param("name") String name,
                            @Param("img_url") String imgUrl,
                            @Param("desc") String desc,
                            @Param("cost_diamond") long costDiamond,
                            @Param("gift_money") long giftMoney,
                            @Param("avaliable") String avaliable) {
        WinesResponse response = null;
        try {
            response = GmIface.instance().iface().updateWines(code, name, imgUrl, desc, costDiamond,
                    giftMoney, avaliable);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(response);
    }

    // 刷新物品到缓存
    public void loadWinesToCache() {
        try {
            GmIface.instance().iface().loadWinesToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }
    }
}
