package com.tiantian.gmback.controller.gm;

import com.tiantian.gm.proxy_client.GmIface;
import org.apache.thrift.TException;
import ro.pippo.controller.Controller;

/**
 *
 */
public class ShopPropsController extends Controller {

    public void loadShopPropsToCache() {
        try {
            GmIface.instance().iface().loadShopPropsToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }

    }
}
