package com.tiantian.gmback.controller.gm;

import com.tiantian.gm.proxy_client.GmIface;
import com.tiantian.gm.thrift.GoodsResponse;
import org.apache.thrift.TException;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class GoodsController extends Controller {
    // 获取所有的物品
     public void getAllGoods() {
         try {
             getResponse().json().send(GmIface.instance().iface().getAllGoods());
         } catch (TException e) {
             e.printStackTrace();
             getResponse().json().send(false);
         }
     }

    // 添加物品
    public void addGood(@Param("code") String code,
                         @Param("name") String name,
                         @Param("img_url") String imgUrl,
                         @Param("desc") String desc) {
        GoodsResponse response = null;
        try {
            response = GmIface.instance().iface().addGoods(code, name, imgUrl, desc);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(response);
    }

    // 更新物品
    public void updateGood(@Param("code") String code,
                            @Param("name") String name,
                            @Param("img_url") String imgUrl,
                            @Param("desc") String desc) {
        GoodsResponse response = null;
        try {
            response = GmIface.instance().iface().updateGoods(code, name, imgUrl, desc);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(response);
    }

    // 刷新物品到缓存
     public void loadGoodsToCache() {
         try {
             GmIface.instance().iface().loadGoodsToCache();
             getResponse().json().send(true);
         } catch (TException e) {
             e.printStackTrace();
             getResponse().json().send(false);
         }
     }
}
