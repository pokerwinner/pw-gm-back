package com.tiantian.gmback.controller.gm;

import com.tiantian.gm.proxy_client.GmIface;
import org.apache.thrift.TException;
import ro.pippo.controller.Controller;

/**
 *
 */
public class SignGiftController extends Controller {

    public void loadSignGiftsToCache() {
        try {
            GmIface.instance().iface().loadSignGiftsToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }
    }

    public void loadSignBonusGiftsToCache() {
        try {
            GmIface.instance().iface().loadSignBonusGiftsToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }
    }
}
