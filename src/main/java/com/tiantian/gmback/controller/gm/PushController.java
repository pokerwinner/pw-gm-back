package com.tiantian.gmback.controller.gm;

import com.tiantian.gmback.utils.PushUtils;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class PushController extends Controller {

    public void pushAll(@Param("content") String content) {
        PushUtils.sendToAll(content);
        getResponse().json().send(true);
    }
}
