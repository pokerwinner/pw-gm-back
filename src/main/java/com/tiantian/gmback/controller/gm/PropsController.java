package com.tiantian.gmback.controller.gm;

import com.tiantian.gm.proxy_client.GmIface;
import com.tiantian.gm.thrift.PropsResponse;
import org.apache.thrift.TException;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class PropsController extends Controller {

    public void addProp(@Param("code") String code,
                        @Param("name") String name,
                        @Param("img_url") String imgUrl,
                        @Param("desc") String desc,
                        @Param("goods_code") String goodsCode,
                        @Param("numbers") long numbers,
                        @Param("cost_diamond") long costDiamond,
                        @Param("cost_money") long costMoney,
                        @Param("avaliable") String avaliable,
                        @Param("order") int order) {
        PropsResponse response = null;
        try {
            response = GmIface.instance().iface().addProps(code, name, imgUrl, desc, goodsCode, numbers, costDiamond,
                    costMoney, avaliable, order);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(response);
    }

    public void updateProp(@Param("code") String code,
                        @Param("name") String name,
                        @Param("img_url") String imgUrl,
                        @Param("desc") String desc,
                        @Param("goods_code") String goodsCode,
                        @Param("numbers") long numbers,
                        @Param("cost_diamond") long costDiamond,
                        @Param("cost_money") long costMoney,
                        @Param("avaliable") String avaliable,
                        @Param("order") int order) {
        PropsResponse response = null;
        try {
            response = GmIface.instance().iface().updateProps(code, name, imgUrl, desc, goodsCode, numbers,
                    costDiamond, costMoney, avaliable, order);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(response);
    }

    public void loadPropsToCache() {
        try {
            GmIface.instance().iface().loadPropsToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }

    }
}
