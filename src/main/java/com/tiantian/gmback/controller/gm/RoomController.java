package com.tiantian.gmback.controller.gm;

import com.tiantian.service.proxy_client.RoomIface;
import com.tiantian.service.thrift.room.RoomDetail;
import org.apache.thrift.TException;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;


/**
 *
 */
public class RoomController extends Controller {

    public void addRoom(@Param("room_name") String roomName,
                        @Param("rule_id") String ruleId,
                        @Param("available") boolean available,
                        @Param("max_carry") long maxCarry) {
        RoomDetail roomDetail = null;
        try {
            roomDetail = RoomIface.instance().iface().createRoom(roomName, available, ruleId,  maxCarry);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(roomDetail);
    }

    public void updateRoom(@Param("room_id") String roomId,
                           @Param("room_name") String roomName,
                           @Param("rule_id") String ruleId,
                           @Param("available") boolean available,
                           @Param("max_carry") long maxCarry,
                           @Param("is_lock") boolean isLock) {
        RoomDetail roomDetail = null;
        try {
            roomDetail = RoomIface.instance().iface().updateRoom(roomId, roomName, available, ruleId, maxCarry, isLock);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(roomDetail);
    }

    public void deleteRoom(@Param("room_id") String roomId) {
        boolean result = false;
        try {
            result = RoomIface.instance().iface().deleteRoom(roomId);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(result);
    }

    public void refresh() {
        boolean result = false;
        try {
            result = RoomIface.instance().iface().refreshRooms();
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(result);
    }

    public void closeRoom(@Param("room_id") String roomId) {
        boolean result = false;
        try {
            result = RoomIface.instance().iface().setRoomAvailable(roomId, false);
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(result);
    }

}
