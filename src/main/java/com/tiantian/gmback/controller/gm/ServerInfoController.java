package com.tiantian.gmback.controller.gm;

import com.google.inject.Inject;
import com.tiantian.gm.proxy_client.GmIface;
import com.tiantian.gmback.service.GmService;
import org.apache.thrift.TException;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class ServerInfoController extends Controller {

    @Inject
    private GmService gmService;

    public void loadVersionsToCache() {
        try {
            GmIface.instance().iface().loadVersionToCache();
        } catch (TException e) {
            e.printStackTrace();
        }
        getResponse().json().send(true);
    }

    public void loadUrlsToCache() {
        try {
            GmIface.instance().iface().loadServerInfoToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }

    }

    public void loadChannlePlatformToCache() {
        try {
            GmIface.instance().iface().loadChannelPlatformToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }
    }

    public void loadAppStatusToCache() {
        try {
            GmIface.instance().iface().loadAppStatusToCache();
            getResponse().json().send(true);
        } catch (TException e) {
            e.printStackTrace();
            getResponse().json().send(false);
        }
    }

    public void setAppStatus(@Param("appName") String appName,
                             @Param("beginDate") long beginDate,
                             @Param("hours") long hours) {
        gmService.setAppStatus(appName, beginDate, hours);
        getResponse().json().send(true);
    }


    public void delAppStatus(@Param("appName") String appName) {
        gmService.delAppStatus(appName);
        getResponse().json().send(true);
    }
}
