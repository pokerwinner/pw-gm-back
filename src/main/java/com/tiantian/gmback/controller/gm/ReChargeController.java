package com.tiantian.gmback.controller.gm;

import com.google.inject.Inject;
import com.tiantian.gmback.service.ReChargeService;
import ro.pippo.controller.Controller;

/**
 *
 */
public class ReChargeController extends Controller {
    @Inject
    private ReChargeService reChargeService;

    public void loadRechargeToCache() {
       boolean ret = reChargeService.loadRechargeToCache();
       getResponse().json().send(ret);
    }
}
