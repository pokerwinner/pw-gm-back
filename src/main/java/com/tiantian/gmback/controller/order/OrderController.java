package com.tiantian.gmback.controller.order;

import com.google.inject.Inject;
import com.tiantian.gmback.controller.ResultStatus;
import com.tiantian.gmback.db.Page;
import com.tiantian.gmback.entity.order.Order;
import com.tiantian.gmback.service.OrderService;
import ro.pippo.controller.Controller;
import ro.pippo.controller.Form;
import ro.pippo.core.Param;

import javax.xml.transform.Result;

/**
 *
 */
public class OrderController extends Controller {

    @Inject
    private OrderService orderService;

    public void orderPage(@Form Page<Order> page,
                          @Form Order search) {
        Page<Order> resultPage = orderService.orderPage(page, search);
        getResponse().json().send(resultPage);
    }

    public void sendJdEcard(@Param("order_id") String orderId, @Param("ecard_code") String ecardCode) {
       ResultStatus resultStatus = orderService.sendJdEcard(orderId, ecardCode);
       getResponse().json().send(resultStatus);
    }
}
