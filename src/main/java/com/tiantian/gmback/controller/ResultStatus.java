package com.tiantian.gmback.controller;

/**
 *
 */
public class ResultStatus {
    private int retCode;
    private String message;

    public ResultStatus() {
    }
    public ResultStatus(int retCode, String message) {
        this.retCode = retCode;
        this.message = message;
    }

    public int getRetCode() {
        return retCode;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
