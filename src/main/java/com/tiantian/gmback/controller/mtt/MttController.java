package com.tiantian.gmback.controller.mtt;

import com.google.inject.Inject;
import com.tiantian.gmback.entity.mtt.MttGame;
import com.tiantian.gmback.entity.mtt.Reward;
import com.tiantian.gmback.service.MttService;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class MttController extends Controller {
    @Inject
    private MttService mttService;

    public void addMtt(@Param("gameName")String gameName,
                       @Param("taxFee")long taxFee,
                       @Param("poolFee")long poolFee,
                       @Param("tableUsers")int tableUsers,
                       @Param("minUsers")int minUsers,
                       @Param("maxUsers")int maxUsers,
                       @Param("startMills")long startMills,
                       @Param("perRestMins")int perRestMins,
                       @Param("restMins")int restMins,
                       @Param("startBuyIn")int startBuyIn,
                       @Param("buyInCnt")int buyInCnt,
                       @Param("rebuyDesc")String rebuyDesc,
                       @Param("canRebuyBlindLvl")int canRebuyBlindLvl,
                       @Param("signDelayMins")int signDelayMins,
                       @Param("rewardMark")String rewardMark,
                       @Param("type")String type,
                       @Param("ruleId")String ruleId,
                       @Param("virtualNums")long[] virtualNums,
                       @Param("physicalIds")String[] physicalIds) {

         MttGame mttGame = mttService.saveMttGame(gameName, taxFee, poolFee,
                                tableUsers, minUsers, maxUsers,
                                startMills, perRestMins, restMins,
                                startBuyIn, buyInCnt, rebuyDesc,
                                canRebuyBlindLvl, signDelayMins,
                                rewardMark, type, ruleId, virtualNums, physicalIds);
         getResponse().json().send(mttGame);
    }

    public void addReward(@Param("rewardName")String rewardName) {
        Reward reward = mttService.addNewReward(rewardName);
        getResponse().json().send(reward);
    }
}
