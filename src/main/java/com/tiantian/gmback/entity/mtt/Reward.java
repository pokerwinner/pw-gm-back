package com.tiantian.gmback.entity.mtt;

/**
 *
 */
public class Reward {
    private String awardId;
    private String awardName;
    private int status;
    private long createTime;
    private int receiveNum;
    private int notReceiveNum;
    public Reward() {
    }
    public Reward(String awardId, String awardName, int status, long createTime,
                  int receiveNum, int notReceiveNum) {
        this.awardId = awardId;
        this.awardName = awardName;
        this.status = status;
        this.createTime = createTime;
        this.receiveNum = receiveNum;
        this.notReceiveNum = notReceiveNum;
    }

    public String getAwardId() {
        return awardId;
    }

    public void setAwardId(String awardId) {
        this.awardId = awardId;
    }

    public String getAwardName() {
        return awardName;
    }

    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getReceiveNum() {
        return receiveNum;
    }

    public void setReceiveNum(int receiveNum) {
        this.receiveNum = receiveNum;
    }

    public int getNotReceiveNum() {
        return notReceiveNum;
    }

    public void setNotReceiveNum(int notReceiveNum) {
        this.notReceiveNum = notReceiveNum;
    }
}
