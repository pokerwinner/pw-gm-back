package com.tiantian.gmback.entity.mtt;

import java.util.List;

/**
 *
 */
public class RuleInfo {
    private String ruleId;
    private List<MttRule> rules;

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public List<MttRule> getRules() {
        return rules;
    }

    public void setRules(List<MttRule> rules) {
        this.rules = rules;
    }
}
