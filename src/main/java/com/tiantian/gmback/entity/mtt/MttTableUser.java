package com.tiantian.gmback.entity.mtt;

/**
 *
 */
public class MttTableUser {
    private String userId;
    private String name;
    private String avatarUrl;
    private int ranking;
    private long leftChips;
    private long joinTime;
    private String status; // waiting, gaming, end
    private int rebuyCnt; // rebuy次数 默认为1

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public long getLeftChips() {
        return leftChips;
    }

    public void setLeftChips(long leftChips) {
        this.leftChips = leftChips;
    }

    public long getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(long joinTime) {
        this.joinTime = joinTime;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRebuyCnt() {
        return rebuyCnt;
    }

    public void setRebuyCnt(int rebuyCnt) {
        this.rebuyCnt = rebuyCnt;
    }
}
