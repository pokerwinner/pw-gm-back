package com.tiantian.gmback.entity.order;

/**
 *
 */
public class Order {
    private String orderId;
    private String userId;
    private String shopPropCode;
    private String shopPropName;
    private String status;  // "1" 处理中 "2" 已发货
    private String addressId;
    private long createDate;
    private long updateDate;
    private String type;

    public Order() {
    }

    public Order(String orderId, String userId, String shopPropCode, String shopPropName, String status,
                 String addressId,
                 long createDate, long updateDate, String type) {
        this.orderId = orderId;
        this.userId = userId;
        this.shopPropCode = shopPropCode;
        this.shopPropName = shopPropName;
        this.status = status;
        this.addressId = addressId;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.type = type;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getShopPropCode() {
        return shopPropCode;
    }

    public void setShopPropCode(String shopPropCode) {
        this.shopPropCode = shopPropCode;
    }

    public String getShopPropName() {
        return shopPropName;
    }

    public void setShopPropName(String shopPropName) {
        this.shopPropName = shopPropName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
