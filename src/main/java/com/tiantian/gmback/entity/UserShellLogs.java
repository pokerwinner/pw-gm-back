package com.tiantian.gmback.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 */
@Document(collection = "user_shell_logs")
public class UserShellLogs {
    @Id
    private String logId;
    private String userId;
    private long shell;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getShell() {
        return shell;
    }

    public void setShell(long shell) {
        this.shell = shell;
    }
}
