package com.tiantian.gmback.entity.logs;

/**
 *
 */
public class JdEcardLogs {
    private String id;
    private String orderId;
    private String userId;
    private String ecardCode; // 加密后保存
    private String secretId; // 加密秘钥的ID
    private long createDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEcardCode() {
        return ecardCode;
    }

    public void setEcardCode(String ecardCode) {
        this.ecardCode = ecardCode;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }
}
