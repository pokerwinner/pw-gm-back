package com.tiantian.gmback.entity;

/**
 * 充值钻石信息
 */
public class ChargeInfo {
    private String chargeId;
    private String title;
    private String content;
    private double amount; // 价格
    private long gitDiamond; // 赠送钻石数量
    private long extraGitDiamond; // 额外赠送钻石数量
    private long limitTimes; // 充值次数限制 -1 不限制, 1 限制一次
    private String imgUrl; // 图片

    public String getChargeId() {
        return chargeId;
    }

    public void setChargeId(String chargeId) {
        this.chargeId = chargeId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getGitDiamond() {
        return gitDiamond;
    }

    public void setGitDiamond(long gitDiamond) {
        this.gitDiamond = gitDiamond;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getExtraGitDiamond() {
        return extraGitDiamond;
    }

    public void setExtraGitDiamond(long extraGitDiamond) {
        this.extraGitDiamond = extraGitDiamond;
    }

    public long getLimitTimes() {
        return limitTimes;
    }

    public void setLimitTimes(long limitTimes) {
        this.limitTimes = limitTimes;
    }
}
