package com.tiantian.gmback.application;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.tiantian.gmback.controller.gm.*;
import com.tiantian.gmback.controller.mtt.MttController;
import com.tiantian.gmback.controller.order.OrderController;
import com.tiantian.gmback.controller.upload.UploadController;
import com.tiantian.gmback.guice.GuiceModule;
import ro.pippo.core.ExceptionHandler;
import ro.pippo.core.route.RouteContext;
import ro.pippo.guice.GuiceControllerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class ControllerApplication  extends ro.pippo.controller.ControllerApplication {
    @Override
    protected void onInit() {
        addPublicResourceRoute();
        addWebjarsResourceRoute();

        Injector injector = Guice.createInjector(new GuiceModule());
        setControllerFactory(new GuiceControllerFactory(injector));
        /** 游戏房间 **/
        POST("/admin/v1/rooms/create", RoomController.class, "addRoom");
        POST("/admin/v1/rooms/{code}/update", RoomController.class, "updateRoom");
        POST("/admin/v1/rooms/refresh", RoomController.class, "refresh");
        POST("/admin/v1/rooms/{code}/delete", RoomController.class, "deleteRoom");
        POST("/admin/v1/rooms/:id/close", RoomController.class, "closeRoom");

        /** 基础物品 **/
        POST("/admin/v1/goods/all", GoodsController.class, "getAllGoods");
        POST("/admin/v1/goods/create", GoodsController.class, "addGood");
        POST("/admin/v1/goods/{code}/update", GoodsController.class, "updateGood");
        POST("/admin/v1/goods/load_cache", GoodsController.class, "loadGoodsToCache");

        /** 美酒 **/
        POST("/admin/v1/wines/create", WinesController.class, "addWine");
        POST("/admin/v1/wines/{code}/update", WinesController.class, "updateWine");
        POST("/admin/v1/wines/load_cache", WinesController.class, "loadWinesToCache");

        /**道具超市**/
        POST("/admin/v1/props/create", PropsController.class, "addProp");
        POST("/admin/v1/props/{code}/update", PropsController.class, "updateProp");
        POST("/admin/v1/props/load_cache", PropsController.class, "loadPropsToCache");

        /**服务器信息**/
        POST("/admin/v1/version/load_cache", ServerInfoController.class, "loadVersionsToCache");
        POST("/admin/v1/urls/load_cache", ServerInfoController.class, "loadUrlsToCache");
        POST("/admin/v1/channle_platform/load_cache", ServerInfoController.class, "loadChannlePlatformToCache");
        POST("/admin/v1/app_status/load_cache", ServerInfoController.class, "loadAppStatusToCache");
        POST("/admin/v1/app_maintain/set", ServerInfoController.class, "setAppStatus");
        POST("/admin/v1/app_maintain/del", ServerInfoController.class, "delAppStatus");
        /**兑换专区**/
        POST("/admin/v1/shop_props/load_cache", ShopPropsController.class, "loadShopPropsToCache");

        /**物品奖励**/
        POST("/admin/v1/gifts/load_sign_gift_cache", SignGiftController.class, "loadSignGiftsToCache");
        POST("/admin/v1/gifts/load_bonus_sign_gift_cache", SignGiftController.class, "loadSignBonusGiftsToCache");

        /** 订单 **/
        POST("/admin/v1/order/page", OrderController.class, "orderPage");
        POST("/admin/v1/order/send_jd_ecard", OrderController.class, "sendJdEcard");

        /**公告**/
        POST("/admin/v1/notice/page", NoticeController.class, "noticePage");
        POST("/admin/v1/notice/create", NoticeController.class, "addNotice");
        POST("/admin/v1/notice/update", NoticeController.class, "updateNotice");
        POST("/admin/v1/notice/{id}/delete", NoticeController.class, "deleteNotice");
        POST("/admin/v1/notice/load_cache", NoticeController.class, "loadAllNoticeToCache");

        /**充值信息**/
        POST("/admin/v1/re_charge/load_cache", ReChargeController.class, "loadRechargeToCache");
        /** **/
        POST("/admin/v1/push/all", PushController.class, "pushAll");

        /** **/
        POST("/admin/v1/upload/log", UploadController.class, "uploadLog");
        /**mtt**/
        POST("/admin/v1/mtt/add_mtt", MttController.class, "addMtt");
        POST("/admin/v1/mtt/add_reward", MttController.class, "addReward");
        getErrorHandler().setExceptionHandler(Exception.class, new ExceptionHandler() {
            @Override
            public void handle(Exception e, RouteContext routeContext) {
                e.printStackTrace();
                routeContext.setLocal("message", e.getMessage());
                Map<String, Integer> map = new HashMap<>();
                map.put("status", 500);
                routeContext.getResponse().json().send(map);
            }
        });
    }
}
