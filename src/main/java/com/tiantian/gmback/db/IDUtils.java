package com.tiantian.gmback.db;

import java.util.UUID;

/**
 *
 */
public class IDUtils {
    public static String UUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
