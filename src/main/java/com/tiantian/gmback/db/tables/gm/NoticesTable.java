package com.tiantian.gmback.db.tables.gm;

/**
 *
 */
public class NoticesTable {
    public static final String TABLE_NOTICE = "public.notices";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title"; //标题
    public static final String COLUMN_CONTENT = "content"; // 内容
    public static final String COLUMN_TAIL = "tail"; // 落款
    public static final String COLUMN_BEGIN_DATE = "begin_date"; // 开始时间
    public static final String COLUMN_END_DATE = "end_date"; // 结束时间
    public static final String COLUMN_AVAILABLE = "available";// 是否可用
    public static final String COLUMN_SHOW_ORDER = "show_order"; //显示顺序
}
