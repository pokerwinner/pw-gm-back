package com.tiantian.gmback.db.tables.logs;

/**
 *
 */
public class JdEcardLogsTable {
    public static final String TABLE_JD_ECARD_LOGS = "public.jd_ecard_logs";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ORDER_ID = "order_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_ECARD_CODE = "ecard_code";
    public static final String COLUMN_SECRET_ID = "secret_id";
    public static final String COLUMN_CREATE_DATE = "create_date";
}
