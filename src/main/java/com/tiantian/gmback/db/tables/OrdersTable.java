package com.tiantian.gmback.db.tables;

/**
 *
 */
public class OrdersTable {
    public static final String TABLE_ORDER = "public.orders";
    public static final String COLUMN_ORDER_ID = "order_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_SHOP_PROP_CODE = "shop_prop_code";
    public static final String COLUMN_SHOP_PROP_NAME = "shop_prop_name";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_ADDRESS_ID = "address_id";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_CREATE_DATE = "create_date";
    public static final String COLUMN_UPDATE_DATE = "update_date";
}
