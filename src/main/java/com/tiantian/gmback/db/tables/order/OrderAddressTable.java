package com.tiantian.gmback.db.tables.order;

/**
 *
 */
public class OrderAddressTable {
    public static final String TABLE_ORDER_ADDRESS = "public.order_address";
    public static final String COLUMN_ADDRESS_ID = "id";
    public static final String COLUMN_RECEIPT_ADDRESS = "receipt_address";
    public static final String COLUMN_ZIPCODE = "zip_code";
    public static final String COLUMN_RECEIVERNAME = "receiver_name";
    public static final String COLUMN_MOBILENUMBER = "mobile_number";
    public static final String COLUMN_TELEPHONENUMBER = "telephone_number";
    public static final String COLUMN_EMAIL = "email";
}
