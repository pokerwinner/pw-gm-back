package com.tiantian.gmback.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.mongodb.Mongo;
import com.tiantian.gmback.db.*;
import com.tiantian.gmback.mongo.CoreMongoTemplateProvider;
import com.tiantian.gmback.mongo.LogMongoTemplateProvider;
import com.tiantian.gmback.mongo.MongoProvider;
import com.tiantian.gmback.redis.RedisTemplateProvider;
import com.tiantian.gmback.service.*;
import com.tiantian.gmback.service.impl.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Properties;

/**
 *
 */
public class GuiceModule extends AbstractModule {
    private Logger LOG = LoggerFactory.getLogger(GuiceModule.class);
    @Override
    protected void configure() {
        bind(DataSource.class).annotatedWith(Names.named("core")).toProvider(DataSourceProvider.class).asEagerSingleton();
        bind(JdbcTemplate.class).to(JdbcTemplateImpl.class).asEagerSingleton();
        bind(OrderService.class).to(OrderServiceImpl.class).asEagerSingleton();
        bind(NoticeService.class).to(NoticeServiceImpl.class).asEagerSingleton();
        bind(ReChargeService.class).to(ReChargeServiceImpl.class).asEagerSingleton();
        bind(GmService.class).to(GmServiceImpl.class).asEagerSingleton();
        bind(MttService.class).to(MttServiceImpl.class).asEagerSingleton();
        bind(Mongo.class).toProvider(MongoProvider.class).asEagerSingleton();
        bind(MongoTemplate.class).annotatedWith(Names.named("mongoCore")).toProvider(CoreMongoTemplateProvider.class)
                .asEagerSingleton();
        bind(MongoTemplate.class).annotatedWith(Names.named("mongoLogs")).toProvider(LogMongoTemplateProvider.class)
                .asEagerSingleton();
        bind(StringRedisTemplate.class).toProvider(RedisTemplateProvider.class).asEagerSingleton();

        configureProperties("/conf/db.properties", "db");
        configureProperties("/conf/redis.properties", "redis");
        configureProperties("/conf/mongo.properties", "mongo");
    }

    private void configureProperties(String fileName, String namePre) {
        Properties p = new Properties();
        try {
            p.load(new InputStreamReader(this.getClass()
                    .getResourceAsStream(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        String env = System.getenv("NOMAD_ENV");
        if(null == env || "" == env){
            env = "development";
        }
        env += ".";
        Enumeration e = p.keys();
        while(e.hasMoreElements()) {
            String key = (String)e.nextElement();
            if (!key.startsWith(env)) {
                continue;
            }
            String value = (String)p.get(key);
            key = key.replace(env, "");
            LOG.info("env is {}, {}, {} : {}",  env, fileName, key, value);
            bindConstant().annotatedWith(Names.named(
                    StringUtils.isNotBlank(namePre) ? namePre + "." + key : key)).to(value);
        }
    }
}
