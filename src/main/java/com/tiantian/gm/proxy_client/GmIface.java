package com.tiantian.gm.proxy_client;

import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.gm.settings.GmConfig;
import com.tiantian.gm.thrift.GmService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;


/**
 *
 */
public class GmIface extends IFace<GmService.Iface>{
    private static class GmIfaceHolder {
        private final static GmIface instance = new GmIface();
    }
    public static GmIface instance() {
        return GmIfaceHolder.instance;
    }


    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new GmService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(GmConfig.getInstance().getHost(),
                GmConfig.getInstance().getPort());
    }

    @Override
    protected Class<GmService.Iface> faceClass() {
        return GmService.Iface.class;
    }
}
